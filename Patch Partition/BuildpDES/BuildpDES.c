//BuildpDES.c
//�5/18/02 David "Potatoswatter" Krauss

#include <SCSI.h>


extern UInt16 DriverChecksum( Handle dH ) {
	UInt32 bcnt;
	UInt32 sum;
	
	sum = 0;
	for ( bcnt = 0; bcnt < GetHandleSize( dH ); bcnt++ ) {
		sum += (UInt8) *( *dH + bcnt );
		sum <<= 1;
		if ( sum & 0x10000 ) sum |= 1;
	}
	
	if ( (UInt16) sum == 0 ) {
		sum = -1;
		PtrToXHand( &sum, dH, 1 );
		
		return DriverChecksum( dH );
	}
	
	return sum;
}


void main( void ) {
	Handle patch;
	PatchDescriptor **pd;
	
	patch = Get1Resource( 'ptch', 128 );
	DetachResource( patch );
	pd = (PatchDescriptor **) Get1Resource( 'pDES', 128 );
	DetachResource( (Handle) pd );
	
	(**pd).patchDescriptorLen = ( GetHandleSize((Handle)pd) + 1 ) & ~1;
	(**pd).patchCRC = DriverChecksum( patch );
	(**pd).patchSize = GetHandleSize( patch );
	
	OpenResFile( "\pEphemerboot Installer" );
	
	RemoveResource( Get1Resource( 'pDES', 128 ) );
	AddResource( (Handle) pd, 'pDES', 128, "\p" );
	
	RemoveResource( Get1Resource( 'ptch', 128 ) );
	AddResource( patch, 'ptch', 128, "\p" );
}