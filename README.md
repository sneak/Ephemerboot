# ephemerboot

ephemerboot by David Krauss

fetched from

https://web.archive.org/web/20060206234036/http://homepage.mac.com/potswa/source/Ephemerboot.sit

# Info

* [https://web.archive.org/web/20060206234036/http://homepage.mac.com/potswa/source/](https://web.archive.org/web/20060206234036/http://homepage.mac.com/potswa/source/)
* [http://macos9lives.com/smforum/index.php?topic=3842.0](http://macos9lives.com/smforum/index.php?topic=3842.0)

# changes in 2019

I added .txt to the two text files in the root because twenty years later we
no longer have type/creator codes or resource forks, and removed an option-8
bullet point from one of the ~~folder~~directory names.

# Copyright

&copy; 5/18/02 - 5/27/02 David "Potatoswatter" Krauss - v1.0.0c1
